ddwt2016
========

Course repository for [Database-drive Web Technology 2016/17](https://www.rug.nl/ocasys/rug//vak/show?code=LIX021B05).

Lecture Slides
--------------

* [Week 1: Basics](http://www.let.rug.nl/evang/teaching/ddwt2016/week1/slides)

Other materials can be found on Nestor.
